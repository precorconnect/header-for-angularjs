## Description
header used by AngularJS based Precorconnect web apps.

## Example
refer to the [example app](example) for working example code.

## Setup

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/header-for-angularjs
``` 

**import & wire up**
```js
import 'header-for-angularjs';

angular.module(
            "app",
            ["header.module"]
        )
        // ensure dependency available in container
        .constant(
            'sessionManager', 
            sessionManager
            /*see https://bitbucket.org/precorconnect/session-manager-for-browsers*/
        );
```

## Configuration 
####Attributes  
| Name (* denotes required) |Type| Description |  
|------|-------|-------------|  
| isLoginRequired | boolean |if set to true, enforces a user is logged in. |  
