import angular from 'angular';
import '../src/module';
import SessionManager,{SessionManagerConfig} from 'session-manager';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

const sessionManagerConfig =
    new SessionManagerConfig(
        precorConnectApiBaseUrl/* identityServiceBaseUrl */,
        'https://precor.oktapreview.com/app/template_saml_2_0/exk5cmdj3pY2eT5JU0h7/sso/saml'/* loginUrl */,
        'https://dev.precorconnect.com/customer/account/logout/'/* logoutUrl*/,
        30000/* accessTokenRefreshInterval */
    );

export default angular
    .module(
        'exampleApp.module',
        [
            'header.module'
        ])
    .constant(
        'sessionManager',
        new SessionManager(sessionManagerConfig)
    );
